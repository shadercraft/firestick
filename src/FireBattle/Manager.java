package FireBattle;
import java.util.ArrayList;

import net.minecraft.server.v1_8_R1.EntityPlayer;

import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.Effect;
import org.bukkit.GameMode;
import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.Sound;
import org.bukkit.block.Block;
import org.bukkit.block.BlockFace;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.enchantments.Enchantment;
import org.bukkit.entity.Entity;
import org.bukkit.entity.EntityType;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.block.Action;
import org.bukkit.event.entity.PlayerDeathEvent;
import org.bukkit.event.player.PlayerInteractEvent;
import org.bukkit.event.player.PlayerJoinEvent;
import org.bukkit.event.player.PlayerRespawnEvent;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;
import org.bukkit.potion.Potion;
import org.bukkit.potion.PotionEffect;
import org.bukkit.potion.PotionEffectType;
import org.bukkit.scoreboard.Scoreboard;
import org.bukkit.scoreboard.ScoreboardManager;
import org.bukkit.scoreboard.Team;
public class Manager implements CommandExecutor, Listener{

	private static FireBattle plugin;
	public Manager(FireBattle plugin){
		this.plugin = plugin;
	}
	static ScoreboardManager manager = Bukkit.getScoreboardManager();
	static Scoreboard board = manager.getNewScoreboard();
	static Team sea = board.registerNewTeam("Sea");
	static Team fire = board.registerNewTeam("Fire");
	
	String st = "�4�lFirer Stick �r�a(�bKlicken um zu Schie�en�a)";
	@Override
	public boolean onCommand(CommandSender sender, Command cmd, String label,String[] args) {
		Player p = (Player)sender;
		
		if (cmd.getName().equalsIgnoreCase("firebattle")){
			if (args.length == 0){
				p.sendMessage(ChatColor.translateAlternateColorCodes('&', "&e[&cFireBattle&e] &bFireBattle wurde Programmiert von &5TheNoim"));
				p.sendMessage(ChatColor.translateAlternateColorCodes('&', "&e[&cFireBattle&e] &bTippe &e/firebattle help &cum eine Liste der Befehle auf zu rufen."));
				
			}
			if (args.length == 1){
				
				if (args[0].equalsIgnoreCase("help")){
					p.sendMessage(ChatColor.translateAlternateColorCodes('&', "&e# # # # # # # &cHelp&e # # # # # # #"));
					p.sendMessage(ChatColor.translateAlternateColorCodes('&', "&e/firebattle reloade &7- &cLade die Conif Datei neu."));
					p.sendMessage(ChatColor.translateAlternateColorCodes('&', "&e/firebattle setsea &7- &cSetze den Spawn Punkt f�r Team &bSea&c."));
					p.sendMessage(ChatColor.translateAlternateColorCodes('&', "&e/firebattle setFire &7- &cSetze den Spawn Punkt f�r Team &4Fire&c."));
					p.sendMessage(ChatColor.translateAlternateColorCodes('&', "&e/firebattle join &bSea&c/&4Fire &7- &cJoin ein Team."));
					p.sendMessage(ChatColor.translateAlternateColorCodes('&', "&e/firebattle leave &bSea&c/&4Fire &7- &cVerlasse ein Team."));
					
				}
				if (args[0].equalsIgnoreCase("setsea")){
					if (p.isOp()){
						this.plugin.getCustomConfig().set("sea.x", p.getLocation().getBlockX());
						this.plugin.getCustomConfig().set("sea.y", p.getLocation().getBlockY());
						this.plugin.getCustomConfig().set("sea.z", p.getLocation().getBlockZ());
						this.plugin.getCustomConfig().set("sea.yaw", p.getLocation().getYaw());
						this.plugin.getCustomConfig().set("sea.pitch", p.getLocation().getPitch());
						this.plugin.getCustomConfig().set("sea.world", p.getWorld().getName());
						this.plugin.saveCustomConfig();
						p.sendMessage(ChatColor.translateAlternateColorCodes('&', "&e[&cFireBattle&e] &cDer Spawn f�r &bSea &cwurde gesetzt."));
					}
				}
				if (args[0].equalsIgnoreCase("setfire")){
					if (p.isOp()){
						this.plugin.getCustomConfig().set("fire.x", p.getLocation().getBlockX());
						this.plugin.getCustomConfig().set("fire.y", p.getLocation().getBlockY());
						this.plugin.getCustomConfig().set("fire.z", p.getLocation().getBlockZ());
						this.plugin.getCustomConfig().set("fire.yaw", p.getLocation().getYaw());
						this.plugin.getCustomConfig().set("fire.pitch", p.getLocation().getPitch());
						this.plugin.getCustomConfig().set("fire.world", p.getWorld().getName());
						this.plugin.saveCustomConfig();
						p.sendMessage(ChatColor.translateAlternateColorCodes('&', "&e[&cFireBattle&e] &cDer Spawn f�r &4Fire &cwurde gesetzt."));
					}
				}
				if (args[0].equalsIgnoreCase("reloade")){
					if (p.isOp()){
						this.plugin.reloadCustomConfig();
						p.sendMessage(ChatColor.translateAlternateColorCodes('&', "&e[&cFireBattle&e] &cDie Config Datei wurde Neugeladen."));
					}
				}
				if (args[0].equalsIgnoreCase("setspawn"))
					if (p.isOp()){
						this.plugin.getCustomConfig().set("spawn.x", p.getLocation().getBlockX());
						this.plugin.getCustomConfig().set("spawn.y", p.getLocation().getBlockY());
						this.plugin.getCustomConfig().set("spawn.z", p.getLocation().getBlockZ());
						this.plugin.getCustomConfig().set("spawn.yaw", p.getLocation().getYaw());
						this.plugin.getCustomConfig().set("spawn.pitch", p.getLocation().getPitch());
						this.plugin.getCustomConfig().set("spawn.world", p.getWorld().getName());
						this.plugin.saveCustomConfig();
						p.sendMessage(ChatColor.translateAlternateColorCodes('&', "&e[&cFireBattle&e] &cDer Spawn wurde gesetzt."));
					}
				}
				
				
			}
			if (args.length == 2){
				if (args[0].equalsIgnoreCase("join")){
					if (args[1].equalsIgnoreCase("Sea")){
						if (sea.getPlayers().contains(p)){
							p.sendMessage(ChatColor.translateAlternateColorCodes('&', "&e[&cFireBattle&e] &cDu bist schon in diesem Team."));
						} else {
							if (fire.getPlayers().contains(p)){
								p.sendMessage(ChatColor.translateAlternateColorCodes('&', "&e[&cFireBattle&e] &cDu bist schon im Team &4Fire&c."));
								p.sendMessage(ChatColor.translateAlternateColorCodes('&', "&e[&cFireBattle&e] &cUm das Team &4Fire&c zu verlassen, Tippe:"));
								p.sendMessage(ChatColor.translateAlternateColorCodes('&', "&e[&cFireBattle&e] &e/firebattle leave Fire"));
							} else {
								sea.addPlayer(p);
								p.sendMessage(ChatColor.translateAlternateColorCodes('&', "&e[&cFireBattle&e] &cDu bist nun im Team &bSea&c."));
								Location locsea = new Location(Bukkit.getWorld(this.plugin.getCustomConfig().getString("sea.world")), this.plugin.getCustomConfig().getDouble("sea.x"), this.plugin.getCustomConfig().getDouble("sea.y"), this.plugin.getCustomConfig().getDouble("sea.z"));
								p.teleport(locsea);
								ItemStack it = new ItemStack(Material.BLAZE_ROD, 1);
							    ItemMeta itm = it.getItemMeta();
							    itm.setDisplayName(this.st);
							    ArrayList<String> lores = new ArrayList();
							    lores.add("Schie�e einen Feuerball!");
							    itm.setLore(lores);
							    it.setItemMeta(itm);
							    p.getInventory().addItem(new ItemStack[] { it });
							    sea.setPrefix(ChatColor.translateAlternateColorCodes('&', "&b"));
								fire.setPrefix(ChatColor.translateAlternateColorCodes('&', "&c"));
								p.getInventory().clear();
								p.getInventory().setChestplate(null);
								p.getInventory().setBoots(null);
								p.updateInventory();
								p.setGameMode(GameMode.ADVENTURE);
								p.addPotionEffect(new PotionEffect(PotionEffectType.DAMAGE_RESISTANCE, 10*20, 127));
								p.setPlayerListName(ChatColor.translateAlternateColorCodes('&', "&b" + p.getName()));
							}
						}
						
					}
					if (args[1].equalsIgnoreCase("Fire")){
						if (fire.getPlayers().contains(p)){
							p.sendMessage(ChatColor.translateAlternateColorCodes('&', "&e[&cFireBattle&e] &cDu bist schon in diesem Team."));
						} else {
							if (sea.getPlayers().contains(p)){
								p.sendMessage(ChatColor.translateAlternateColorCodes('&', "&e[&cFireBattle&e] &cDu bist schon im Team &bSea&c."));
								p.sendMessage(ChatColor.translateAlternateColorCodes('&', "&e[&cFireBattle&e] &cUm das Team &bSea&c zu verlassen, Tippe:"));
								p.sendMessage(ChatColor.translateAlternateColorCodes('&', "&e[&cFireBattle&e] &e/firebattle leave Sea"));
							} else {
								fire.addPlayer(p);
								p.sendMessage(ChatColor.translateAlternateColorCodes('&', "&e[&cFireBattle&e] &cDu bist nun im Team &4Fire&c."));
								Location locfire = new Location(Bukkit.getWorld(this.plugin.getCustomConfig().getString("fire.world")), this.plugin.getCustomConfig().getDouble("fire.x"), this.plugin.getCustomConfig().getDouble("fire.y"), this.plugin.getCustomConfig().getDouble("fire.z"));
								p.teleport(locfire);
								ItemStack it = new ItemStack(Material.BLAZE_ROD, 1);
							    ItemMeta itm = it.getItemMeta();
							    itm.setDisplayName(this.st);
							    ArrayList<String> lores = new ArrayList();
							    lores.add("Schie�e einen Feuerball!");
							    itm.setLore(lores);
							    it.setItemMeta(itm);
							    p.getInventory().addItem(new ItemStack[] { it });
							    sea.setPrefix(ChatColor.translateAlternateColorCodes('&', "&b"));
								fire.setPrefix(ChatColor.translateAlternateColorCodes('&', "&c"));
								p.getInventory().clear();
								p.getInventory().setChestplate(null);
								p.getInventory().setBoots(null);
								p.updateInventory();
								p.setGameMode(GameMode.ADVENTURE);
								p.addPotionEffect(new PotionEffect(PotionEffectType.DAMAGE_RESISTANCE, 10*20, 127));
								p.setPlayerListName(ChatColor.translateAlternateColorCodes('&', "&c" + p.getName()));
							}
						}
						
					}
					
				} 
				if (args[0].equalsIgnoreCase("leave")){
					if (args[1].equalsIgnoreCase("fire")){
						if (fire.getPlayers().contains(p)){
							fire.removePlayer(p);
							p.getInventory().clear();
							Location locspawn = new Location(Bukkit.getWorld(this.plugin.getCustomConfig().getString("spawn.world")), this.plugin.getCustomConfig().getDouble("spawn.x"), this.plugin.getCustomConfig().getDouble("spawn.y"), this.plugin.getCustomConfig().getDouble("spawn.z"));
							p.teleport(locspawn);
							p.sendMessage(ChatColor.translateAlternateColorCodes('&', "&e[&cFireBattle&e] &cDu hast das Team &4Fire&c verlassen."));
							p.setGameMode(GameMode.ADVENTURE);
							p.setPlayerListName(ChatColor.translateAlternateColorCodes('&', "&r" + p.getName()));
						} else {
							p.sendMessage(ChatColor.translateAlternateColorCodes('&', "&e[&cFireBattle&e] &cDu bist nicht im Team &4Fire&c."));
						}
					}
					if (args[1].equalsIgnoreCase("sea")){
						if (sea.getPlayers().contains(p)){
							sea.removePlayer(p);
							p.getInventory().clear();
							Location locspawn = new Location(Bukkit.getWorld(this.plugin.getCustomConfig().getString("spawn.world")), this.plugin.getCustomConfig().getDouble("spawn.x"), this.plugin.getCustomConfig().getDouble("spawn.y"), this.plugin.getCustomConfig().getDouble("spawn.z"));
							p.teleport(locspawn);
							p.sendMessage(ChatColor.translateAlternateColorCodes('&', "&e[&cFireBattle&e] &cDu hast das Team &bSae&c verlassen."));
							p.setGameMode(GameMode.ADVENTURE);
							p.setPlayerListName(ChatColor.translateAlternateColorCodes('&', "&r" + p.getName()));
						} else {
							p.sendMessage(ChatColor.translateAlternateColorCodes('&', "&e[&cFireBattle&e] &cDu bist nicht im Team &bSea&c."));
						}
					}
				}
			}
			
			
			
			
			
		
		
		
		
		
		
		return true;
	}
	@EventHandler
	public void onJoin(PlayerJoinEvent e){
		e.setJoinMessage(ChatColor.translateAlternateColorCodes('&', "&9" + e.getPlayer().getDisplayName() + " &chat den Server betreten."));
		Location locspawn = new Location(Bukkit.getWorld(this.plugin.getCustomConfig().getString("spawn.world")), this.plugin.getCustomConfig().getDouble("spawn.x"), this.plugin.getCustomConfig().getDouble("spawn.y"), this.plugin.getCustomConfig().getDouble("spawn.z"));
		final Player p = e.getPlayer();
		p.teleport(locspawn);
		if (fire.getPlayers().contains(p)){
			fire.removePlayer(p);
		}
		if (sea.getPlayers().contains(p)){
			sea.removePlayer(p);
		}
		p.getLocation().getWorld().playSound(p.getLocation(), Sound.ANVIL_LAND, 1, 1);
		sea.setPrefix(ChatColor.translateAlternateColorCodes('&', "&b"));
		fire.setPrefix(ChatColor.translateAlternateColorCodes('&', "&c"));
		p.getInventory().clear();
		p.setGameMode(GameMode.ADVENTURE);
		p.setHealth(20);
		p.setFoodLevel(20);
		p.setPlayerListName(ChatColor.translateAlternateColorCodes('&', "&r" + p.getName()));
		Bukkit.getScheduler().scheduleSyncDelayedTask(this.plugin, new Runnable(){

			@Override
			public void run() {
				p.setGameMode(GameMode.ADVENTURE);
				
			}
			
		}, 10L);
	}
	@EventHandler
	public void onDeath(PlayerDeathEvent e){
		Player p = e.getEntity();
		Entity k = p.getKiller();
		if (e.getEntity().getKiller().getType() == EntityType.FIREBALL || e.getEntity().getKiller().getType() == EntityType.PRIMED_TNT){
			if (k.getCustomName() != null){
				e.setDeathMessage(ChatColor.translateAlternateColorCodes('&',"&9" + p.getName() + " &cwurde von" + " &2" + k.getCustomName() + " &cgekillt."));
			}
			
			
			
			
			
			
		}
		if (k.getType() == EntityType.PLAYER){
			e.setDeathMessage(ChatColor.translateAlternateColorCodes('&',"&9" + p.getName() + " &cwurde von&2 " + k.getCustomName() + " &cgekillt, mit einem" + Bukkit.getPlayer(k.getCustomName()).getItemInHand().getType().toString()) + "&c.");
		}
		
		
		
	}
	@EventHandler
	public void onInteract(PlayerInteractEvent e){
		Player p = e.getPlayer();
		if (e.getAction() == Action.RIGHT_CLICK_BLOCK){
			if (e.getClickedBlock().getType() == Material.STONE_BUTTON){
				Block block = e.getClickedBlock();
				Block face = block.getRelative(BlockFace.EAST);
				if (block != null){
					
				}if (face.getType() == Material.WOOL){
					if (face.getData() == 11){
						if (sea.getPlayers().contains(p)){
							p.sendMessage(ChatColor.translateAlternateColorCodes('&', "&e[&cFireBattle&e] &cDu bist schon in diesem Team."));
						} else {
							if (fire.getPlayers().contains(p)){
								p.sendMessage(ChatColor.translateAlternateColorCodes('&', "&e[&cFireBattle&e] &cDu bist schon im Team &4Fire&c."));
								p.sendMessage(ChatColor.translateAlternateColorCodes('&', "&e[&cFireBattle&e] &cUm das Team &4Fire&c zu verlassen, Tippe:"));
								p.sendMessage(ChatColor.translateAlternateColorCodes('&', "&e[&cFireBattle&e] &e/firebattle leave Fire"));
							} else {
								sea.addPlayer(p);
								p.sendMessage(ChatColor.translateAlternateColorCodes('&', "&e[&cFireBattle&e] &cDu bist nun im Team &bSea&c."));
								Location locsea = new Location(Bukkit.getWorld(this.plugin.getCustomConfig().getString("sea.world")), this.plugin.getCustomConfig().getDouble("sea.x"), this.plugin.getCustomConfig().getDouble("sea.y"), this.plugin.getCustomConfig().getDouble("sea.z"));
								p.teleport(locsea);
								ItemStack it = new ItemStack(Material.BLAZE_ROD, 1);
							    ItemMeta itm = it.getItemMeta();
							    itm.setDisplayName(this.st);
							    ArrayList<String> lores = new ArrayList();
							    lores.add("Schie�e einen Feuerball!");
							    itm.setLore(lores);
							    it.setItemMeta(itm);
							    p.getInventory().addItem(new ItemStack[] { it });
							    sea.setPrefix(ChatColor.translateAlternateColorCodes('&', "&b"));
								fire.setPrefix(ChatColor.translateAlternateColorCodes('&', "&c"));
								p.getInventory().clear();
								p.getInventory().setChestplate(null);
								p.getInventory().setBoots(null);
								p.updateInventory();
								p.setPlayerListName(ChatColor.translateAlternateColorCodes('&', "&b" + p.getName()));
								p.setGameMode(GameMode.ADVENTURE);
								p.addPotionEffect(new PotionEffect(PotionEffectType.DAMAGE_RESISTANCE, 10*20, 127));
							}
						}
					}
					if (face.getData() == 14){
						if (fire.getPlayers().contains(p)){
							p.sendMessage(ChatColor.translateAlternateColorCodes('&', "&e[&cFireBattle&e] &cDu bist schon in diesem Team."));
						} else {
							if (sea.getPlayers().contains(p)){
								p.sendMessage(ChatColor.translateAlternateColorCodes('&', "&e[&cFireBattle&e] &cDu bist schon im Team &bSea&c."));
								p.sendMessage(ChatColor.translateAlternateColorCodes('&', "&e[&cFireBattle&e] &cUm das Team &bSea&c zu verlassen, Tippe:"));
								p.sendMessage(ChatColor.translateAlternateColorCodes('&', "&e[&cFireBattle&e] &e/firebattle leave Sea"));
							} else {
								fire.addPlayer(p);
								p.sendMessage(ChatColor.translateAlternateColorCodes('&', "&e[&cFireBattle&e] &cDu bist nun im Team &4Fire&c."));
								Location locfire = new Location(Bukkit.getWorld(this.plugin.getCustomConfig().getString("fire.world")), this.plugin.getCustomConfig().getDouble("fire.x"), this.plugin.getCustomConfig().getDouble("fire.y"), this.plugin.getCustomConfig().getDouble("fire.z"));
								p.teleport(locfire);
								ItemStack it = new ItemStack(Material.BLAZE_ROD, 1);
								p.setPlayerListName(ChatColor.translateAlternateColorCodes('&', "&c" + p.getName()));
							    ItemMeta itm = it.getItemMeta();
							    itm.setDisplayName(this.st);
							    ArrayList<String> lores = new ArrayList();
							    lores.add("Schie�e einen Feuerball!");
							    itm.setLore(lores);
							    it.setItemMeta(itm);
							    p.getInventory().addItem(new ItemStack[] { it });
							    sea.setPrefix(ChatColor.translateAlternateColorCodes('&', "&b"));
								fire.setPrefix(ChatColor.translateAlternateColorCodes('&', "&c"));
								p.getInventory().clear();
								p.getInventory().setChestplate(null);
								p.getInventory().setBoots(null);
								p.updateInventory();
								p.setGameMode(GameMode.ADVENTURE);
								p.addPotionEffect(new PotionEffect(PotionEffectType.DAMAGE_RESISTANCE, 10*20, 127));
							}
						}
					}
				
				}
				
			}
			if (e.getClickedBlock().getType() == Material.TRAPPED_CHEST && e.getClickedBlock() != null){
				e.setCancelled(true);
				p.getInventory().clear();
				e.getClickedBlock().getWorld().playEffect(e.getClickedBlock().getLocation(), Effect.STEP_SOUND, e.getClickedBlock().getTypeId());
				ItemStack it = new ItemStack(Material.BLAZE_ROD, 1);
			    ItemMeta itm = it.getItemMeta();
			    itm.setDisplayName(this.st);
			    ArrayList<String> lores = new ArrayList();
			    lores.add("Schie�e einen Feuerball!");
			    itm.setLore(lores);
			    it.setItemMeta(itm);
			    p.getInventory().addItem(new ItemStack[] { it });
			    ItemStack steak = new ItemStack(Material.COOKED_BEEF, 64);
			    ItemMeta steakmeta = steak.getItemMeta();
			    steakmeta.setDisplayName(ChatColor.translateAlternateColorCodes('&', "&4&lBEEEEEEF !"));
			    steak.setItemMeta(steakmeta);
			    p.getInventory().addItem(new ItemStack[] { steak });
			    ItemStack Chestplate = new ItemStack(Material.CHAINMAIL_CHESTPLATE, 1);
			    ItemMeta chainmeta = Chestplate.getItemMeta();
			    chainmeta.setDisplayName(ChatColor.translateAlternateColorCodes('&', "&l&6CHAIN!"));
			    chainmeta.addEnchant(Enchantment.PROTECTION_EXPLOSIONS, 1000, true);
			    chainmeta.addEnchant(Enchantment.PROTECTION_ENVIRONMENTAL, 5, true);
			    Chestplate.setItemMeta(chainmeta);
			    p.getInventory().setChestplate(Chestplate);
			    ItemStack boots = new ItemStack(Material.DIAMOND_BOOTS, 1);
			    ItemMeta bootsmeta = boots.getItemMeta();
			    bootsmeta.setDisplayName(ChatColor.translateAlternateColorCodes('&', "&4&lBOOOOOOOOOOOOOOTS!"));
			    bootsmeta.addEnchant(Enchantment.PROTECTION_FALL, 20, true);
			    bootsmeta.addEnchant(Enchantment.PROTECTION_EXPLOSIONS, 127, true);
			    boots.setItemMeta(bootsmeta);
			    p.getInventory().setBoots(boots);
			    ItemStack Sword = new ItemStack(Material.WOOD_SWORD, 1);
			    ItemMeta swordmeta = Sword.getItemMeta();
			    swordmeta.setDisplayName(ChatColor.translateAlternateColorCodes('&', "&BDILDO!"));
			    swordmeta.addEnchant(Enchantment.DAMAGE_ALL, 5, true);
			    Sword.setItemMeta(swordmeta);
			    p.getInventory().addItem(Sword);
			    
			    
			    
			    
			    p.updateInventory();
			}
		}
	}
	@EventHandler
	public void onRespawn(PlayerRespawnEvent e){
		final Player p = e.getPlayer();
		if (fire.getPlayers().contains(p)){
			Location locfire = new Location(Bukkit.getWorld(this.plugin.getCustomConfig().getString("fire.world")), this.plugin.getCustomConfig().getDouble("fire.x"), this.plugin.getCustomConfig().getDouble("fire.y"), this.plugin.getCustomConfig().getDouble("fire.z"));
			e.setRespawnLocation(locfire);
			p.getInventory().clear();
			p.getInventory().setChestplate(null);
			p.getInventory().setBoots(null);
			p.updateInventory();
			p.addPotionEffect(new PotionEffect(PotionEffectType.DAMAGE_RESISTANCE, 10*20, 127));
			p.setGameMode(GameMode.ADVENTURE);
			Bukkit.getScheduler().scheduleSyncDelayedTask(this.plugin, new Runnable(){

				@Override
				public void run() {
					p.addPotionEffect(new PotionEffect(PotionEffectType.DAMAGE_RESISTANCE, 10*20, 127));
					
				}
				
			}, 10L);
		} else {
			if (sea.getPlayers().contains(p)){
				Location locsea = new Location(Bukkit.getWorld(this.plugin.getCustomConfig().getString("sea.world")), this.plugin.getCustomConfig().getDouble("sea.x"), this.plugin.getCustomConfig().getDouble("sea.y"), this.plugin.getCustomConfig().getDouble("sea.z"));
			    e.setRespawnLocation(locsea);
			    p.getInventory().clear();
			    p.getInventory().setChestplate(null);
				p.getInventory().setBoots(null);
				p.updateInventory();
				p.addPotionEffect(new PotionEffect(PotionEffectType.DAMAGE_RESISTANCE, 10*20, 127));
				p.setGameMode(GameMode.ADVENTURE);
				Bukkit.getScheduler().scheduleSyncDelayedTask(this.plugin, new Runnable(){

					@Override
					public void run() {
						p.addPotionEffect(new PotionEffect(PotionEffectType.DAMAGE_RESISTANCE, 10*20, 127));
						
					}
					
				}, 10L);
			} else {
				Location locspawn = new Location(Bukkit.getWorld(this.plugin.getCustomConfig().getString("spawn.world")), this.plugin.getCustomConfig().getDouble("spawn.x"), this.plugin.getCustomConfig().getDouble("spawn.y"), this.plugin.getCustomConfig().getDouble("spawn.z"));
			    e.setRespawnLocation(locspawn);
			    p.getInventory().clear();
			    p.getInventory().setChestplate(null);
				p.getInventory().setBoots(null);
				p.updateInventory();
				p.setGameMode(GameMode.ADVENTURE);
			}
		}
		
		
	}
	

}
