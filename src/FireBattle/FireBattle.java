package FireBattle;

import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.util.logging.Level;
import java.util.logging.Logger;

import org.bukkit.Bukkit;
import org.bukkit.command.CommandExecutor;
import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.configuration.file.YamlConfiguration;
import org.bukkit.plugin.Plugin;
import org.bukkit.plugin.java.JavaPlugin;


public class FireBattle extends JavaPlugin{


	public void onEnable(){
		Bukkit.getPluginManager().registerEvents(new Explosion(this), this);
		getCommand("firebattle").setExecutor(new Manager(this));
		getCommand("firestick").setExecutor(new Stick(this));
		Bukkit.getPluginManager().registerEvents(new Stick(this), this);
		Bukkit.getPluginManager().registerEvents(new Manager(this), this);
		
	}
	
	public void onDisable(){
		
	}
	
	private FileConfiguration customConfig = null;
	private File customConfigurationFile = null;
	
	public void reloadCustomConfig() {
	    if (customConfigurationFile == null) {
	    customConfigurationFile = new File(getDataFolder(), "firebattleconfig.yml");
	    }
	    customConfig = YamlConfiguration.loadConfiguration(customConfigurationFile);
	    InputStream defConfigStream = getResource("firebattleconfig.yml");
	    if (defConfigStream != null) {
	        YamlConfiguration defConfig = YamlConfiguration.loadConfiguration(defConfigStream);
	        customConfig.setDefaults(defConfig);
	    }
	}
	public FileConfiguration getCustomConfig() {
	    if (customConfig == null) {
	        reloadCustomConfig();
	    }
	    return customConfig;
	}
	public void saveCustomConfig() {
	    if (customConfig == null || customConfigurationFile == null) {
	    return;
	    }
	    try {
	        customConfig.save(customConfigurationFile);
	    } catch (IOException ex) {
	        Logger.getLogger(JavaPlugin.class.getName()).log(Level.SEVERE, "Konfiguration konnte nicht nach " + customConfigurationFile + " geschrieben werden.", ex);
	    }
	}
}
