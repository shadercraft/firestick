package FireBattle;

import java.util.ArrayList;





import org.bukkit.Bukkit;
import org.bukkit.Color;
import org.bukkit.DyeColor;
import org.bukkit.FireworkEffect;
import org.bukkit.Material;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Fireball;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.block.Action;
import org.bukkit.event.player.PlayerInteractEvent;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;
import org.bukkit.metadata.MetadataValue;

public class Stick implements Listener, CommandExecutor{

	private static final MetadataValue Ursprung = null;
	private FireBattle plugin;
	  
	  public Stick(FireBattle plugin)
	  {
	    this.plugin = plugin;
	  }
	String st = "�4�lFirer Stick �r�a(�bKlicken um zu Schie�en�a)";
	String st2 = "FIRESTICK!";
	@EventHandler
	public void onInter(PlayerInteractEvent ev){
		ItemStack it = ev.getItem();
	    Action ac = ev.getAction();
	    Player p = ev.getPlayer();
	    if (ac == Action.LEFT_CLICK_AIR || ac == Action.LEFT_CLICK_BLOCK || ac == Action.RIGHT_CLICK_AIR || ac == Action.RIGHT_CLICK_BLOCK){
	    	if (it.getType() == Material.BLAZE_ROD && it != null){
	    		if (it.getItemMeta().getDisplayName().contains(this.st) || it.getItemMeta().getDisplayName().contains(this.st2)){
	    			final Fireball fb = (Fireball)p.launchProjectile(Fireball.class);
	    		    fb.setIsIncendiary(false);
	    		    fb.setYield(0.0F);
	    		    fb.setFireTicks(4);
	    		    fb.setCustomName(p.getName());
	    		    fb.setCustomNameVisible(true);
	    		      
	    			Bukkit.getScheduler().scheduleSyncRepeatingTask(this.plugin, new Runnable(){

						@Override
						public void run() {
							if (!fb.isDead()){
								//FireworkEffect.Builder builder = FireworkEffect.builder();
								//FireworkEffect effect = builder.trail(true).flicker(false).withColor(DyeColor.YELLOW.getFireworkColor()).withColor(DyeColor.BLACK.getFireworkColor()).withColor(DyeColor.ORANGE.getFireworkColor()).withColor(DyeColor.RED.getFireworkColor()).with(FireworkEffect.Type.BURST).build();
								//CustomEntityFirework.spawn(fb.getLocation(), effect);
								FireworkEffectPlayer fplayer = new FireworkEffectPlayer();
								try {
									fplayer.playFirework(fb.getWorld(), fb.getLocation(), FireworkEffect.builder().trail(true).flicker(false).withColor(DyeColor.YELLOW.getFireworkColor()).withColor(DyeColor.BLACK.getFireworkColor()).withColor(DyeColor.ORANGE.getFireworkColor()).withColor(DyeColor.RED.getFireworkColor()).with(FireworkEffect.Type.BURST).build());
								} catch (IllegalArgumentException e) {
									// TODO Auto-generated catch block
									e.printStackTrace();
								} catch (Exception e) {
									// TODO Auto-generated catch block
									e.printStackTrace();
								}
							}
							
						}
	    				  
	    			 }, 1l, 1l);
	    			  
	    		}
	    	}
	    }
	}
	public boolean onCommand(CommandSender sender, Command cmd, String label, String[] args)
	  {
	    if (cmd.getName().equalsIgnoreCase("firestick"))
	    {
	      Player p = (Player)sender;
	      ItemStack it = new ItemStack(Material.BLAZE_ROD, 1);
	      ItemMeta itm = it.getItemMeta();
	      itm.setDisplayName(this.st);
	      ArrayList<String> lores = new ArrayList();
	      lores.add("Schie�e einen Feuerball!");
	      itm.setLore(lores);
	      it.setItemMeta(itm);
	      p.getInventory().addItem(new ItemStack[] { it });
	    }
	    return true;
}
	
	
}
